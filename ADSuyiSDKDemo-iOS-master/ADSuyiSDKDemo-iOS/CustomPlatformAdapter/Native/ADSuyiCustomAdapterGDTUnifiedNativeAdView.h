//
//  ADSuyiCustomAdapterTestUnifiedNativeAdView.h
//  ADSuyiCustomPlatform
//
//  Created by Erik on 2021/9/7.
//

#import <ADSuyiSDK/ADSuyiCustomAdapterUnifiedNativeAdView.h>
#import <ADSuyiSDK/ADSuyiAdapterNativeAdData.h>
#import <GDTMobSDK/GDTUnifiedNativeAdView.h>
#import <GDTMobSDK/GDTUnifiedNativeAdDataObject.h>
NS_ASSUME_NONNULL_BEGIN

@class ADSuyiCustomAdapterGDTUnifiedNativeAdView;
@protocol ADSuyiCustomAdapterGDTUnifiedNativeAdViewDelegate <NSObject>

- (void)adsyCustomUnifiedNativeAdViewRender:(ADSuyiCustomAdapterGDTUnifiedNativeAdView *)adView;

- (void)adsyCustomUnifiedNativeAdViewClose:(ADSuyiCustomAdapterGDTUnifiedNativeAdView *)adView;

@end

@interface ADSuyiCustomAdapterGDTUnifiedNativeAdView : GDTUnifiedNativeAdView<ADSuyiAdapterNativeAdViewDelegate>

@property (nonatomic, strong) GDTUnifiedNativeAdDataObject *adData;

@property (nonatomic, weak) id<ADSuyiCustomAdapterGDTUnifiedNativeAdViewDelegate> unifiedNativeAdDelegate;

@end

NS_ASSUME_NONNULL_END
