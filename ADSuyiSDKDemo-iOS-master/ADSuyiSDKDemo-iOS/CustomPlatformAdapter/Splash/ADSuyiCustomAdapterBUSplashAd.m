//
//  ADSuyiCustomAdapterTestSplashAd.m
//  ADSuyiCustomPlatform
//
//  Created by Erik on 2021/8/28.
//

#import "ADSuyiCustomAdapterBUSplashAd.h"
#import <BUAdSDK/BUAdSDK.h>

@interface ADSuyiCustomAdapterBUSplashAd ()<BUSplashAdDelegate>

@property (nonatomic, strong) BUSplashAd *splashAd;
@property (nonatomic, strong) ADSuyiCustomAdapterSplashRequestContext *context;

@end

@implementation ADSuyiCustomAdapterBUSplashAd

// MARK: - Override

+ (void)load {
    [self registPlatformAdLoaderClass:self forSdkName:@"toutiao"];
}

/// 请求
- (void)requestAdWithContext:(ADSuyiCustomAdapterSplashRequestContext *)context{
    _context = context;
    if(!_splashAd) {
        CGFloat width = UIScreen.mainScreen.bounds.size.width;
        CGFloat height = UIScreen.mainScreen.bounds.size.height - context.bottomView.frame.size.height;
        
        _splashAd = [[BUSplashAd alloc] initWithSlotID:context.posId adSize:CGSizeMake(width, height)];
        _splashAd.delegate = self;
        _splashAd.tolerateTimeout = 3;
    }
    [_splashAd loadAdData];
}

/// 展示
-(void)customAdapter_onAdReceive{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [_splashAd showSplashViewInRootViewController:keyWindow.rootViewController];
    
    UIView *bottomView = _context.bottomView;
    if(bottomView){
        bottomView.frame = CGRectMake(0, UIScreen.mainScreen.bounds.size.height - bottomView.frame.size.height, UIScreen.mainScreen.bounds.size.width, bottomView.frame.size.height);
        [_splashAd.splashRootViewController.view addSubview:bottomView];
    }
}

// MARK: - BUSplashAdDelegate

/// This method is called when material load successful
- (void)splashAdLoadSuccess:(BUSplashAd *)splashAd{
    [self trackSplashAdSucceed];
}

/// This method is called when material load failed
- (void)splashAdLoadFail:(BUSplashAd *)splashAd error:(BUAdError *)error {
    [self trackSplashAdFailedWithError:error];
}

/// This method is called when splash view render successful
- (void)splashAdRenderSuccess:(nonnull BUSplashAd *)splashAd {
    // 渲染成功再展示视图控制器,目前统一放置需要展示的时候
}

/// This method is called when splash view render failed
- (void)splashAdRenderFail:(BUSplashAd *)splashAd error:(BUAdError *)error {
    [self trackSplashAdFailedToPresentWithError:error];
}

/// This method is called when splash view will show
- (void)splashAdWillShow:(nonnull BUSplashAd *)splashAd {}

/// This method is called when splash view did show
- (void)splashAdDidShow:(BUSplashAd *)splashAd {
    [self trackSplashAdSuccessToPresent];
    [self trackSplashAdDisplay];
}

/// This method is called when splash view is clicked.
- (void)splashAdDidClick:(BUSplashAd *)splashAd {
    [self trackSplashAdClicked];
}

/// This method is called when splash view is closed.
- (void)splashAdDidClose:(BUSplashAd *)splashAd closeType:(BUSplashAdCloseType)closeType {
    if(_context.bottomView){
        [_context.bottomView removeFromSuperview];
    }
    if (closeType == BUSplashAdCloseType_ClickSkip) {
        [self trackSplashAdSkip];
    }
    [self trackSplashAdClosed];
}

/// This method is called when splash viewControllr is closed.
- (void)splashAdViewControllerDidClose:(nonnull BUSplashAd *)splashAd {}

/**
 This method is called when another controller has been closed.
 @param interactionType : open appstore in app or open the webpage or view video ad details page.
 */
- (void)splashDidCloseOtherController:(BUSplashAd *)splashAd interactionType:(BUInteractionType)interactionType {
    [self trackSplashAdLandingPageClosed];
}

/// This method is called when when video ad play completed or an error occurred.
- (void)splashVideoAdDidPlayFinish:(nonnull BUSplashAd *)splashAd didFailWithError:(nonnull NSError *)error {}


@end
