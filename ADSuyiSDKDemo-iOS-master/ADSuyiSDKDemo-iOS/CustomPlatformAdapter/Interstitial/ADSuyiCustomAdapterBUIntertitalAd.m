//
//  ADSuyiCustomAdapterTestIntertitalAd.m
//  ADSuyiCustomPlatform
//
//  Created by Erik on 2021/9/6.
//

#import "ADSuyiCustomAdapterBUIntertitalAd.h"
#import <BUAdSDK/BUAdSDK.h>
@interface ADSuyiCustomAdapterBUIntertitalAd ()<BUNativeExpressFullscreenVideoAdDelegate>
{
    BUNativeExpressFullscreenVideoAd *_interstitialAd;
    ADSuyiCustomAdapterInterstitalRequestContext *_context;
}
@end

@implementation ADSuyiCustomAdapterBUIntertitalAd

// MARK: - Override

+ (void)load {
    [self registPlatformAdLoaderClass:self forSdkName:@"toutiao" renderType:(ADSuyiAdapterRenderTypeExpress)];
    [self registPlatformAdLoaderClass:self forSdkName:@"toutiao" renderType:(ADSuyiAdapterRenderTypeExpressPro)];
}

- (void)requestAdWithContext:(ADSuyiCustomAdapterInterstitalRequestContext *)context {
    _context = context;
    
    if(!_interstitialAd) {
        _interstitialAd = [[BUNativeExpressFullscreenVideoAd alloc]initWithSlotID:context.posId];
        _interstitialAd.delegate = self;
    }
    
    [_interstitialAd loadAdData];
}

- (void)showInterstitalAdFromViewController:(UIViewController *)viewController {
    if (!viewController) {
        viewController = _context.viewController;
    }
    [_interstitialAd showAdFromRootViewController:viewController];
}

// MARK: - BUNativeExpressFullscreenVideoAdDelegate

/**
 This method is called when video ad material loaded successfully.
 */
- (void)nativeExpressFullscreenVideoAdDidLoad:(BUNativeExpressFullscreenVideoAd *)fullscreenVideoAd {}

/**
 This method is called when video cached successfully.
 For a better user experience, it is recommended to display video ads at this time.
 And you can call [BUNativeExpressFullscreenVideoAd showAdFromRootViewController:].
 */
- (void)nativeExpressFullscreenVideoAdDidDownLoadVideo:(BUNativeExpressFullscreenVideoAd *)fullscreenVideoAd {
    [self trackInterstitialAdSuccessToLoad];
}

/**
 This method is called when video ad materia failed to load.
 @param error : the reason of error
 */
- (void)nativeExpressFullscreenVideoAd:(BUNativeExpressFullscreenVideoAd *)fullscreenVideoAd didFailWithError:(NSError *_Nullable)error {
    [self trackInterstitialAdFailToLoadError:error];
}

/**
 This method is called when rendering a nativeExpressAdView successed.
 It will happen when ad is show.
 */
- (void)nativeExpressFullscreenVideoAdViewRenderSuccess:(BUNativeExpressFullscreenVideoAd *)rewardedVideoAd {}

/**
 This method is called when a nativeExpressAdView failed to render.
 @param error : the reason of error
 */
- (void)nativeExpressFullscreenVideoAdViewRenderFail:(BUNativeExpressFullscreenVideoAd *)rewardedVideoAd error:(NSError *_Nullable)error {}

- (void)nativeExpressFullscreenVideoAdDidVisible:(BUNativeExpressFullscreenVideoAd *)fullscreenVideoAd {
    [self trackInterstitialAdDidPresent];
    [self trackInterstitialAdDidExposure];
}

- (void)nativeExpressFullscreenVideoAdDidClick:(BUNativeExpressFullscreenVideoAd *)fullscreenVideoAd {
    [self trackInterstitialAdDidClick];
}

- (void)nativeExpressFullscreenVideoAdDidClose:(BUNativeExpressFullscreenVideoAd *)fullscreenVideoAd {
    [self trackInterstitialAdDidClose];
}

- (void)nativeExpressFullscreenVideoAdDidCloseOtherController:(BUNativeExpressFullscreenVideoAd *)fullscreenVideoAd interactionType:(BUInteractionType)interactionType {
    [self trackInterstitialAdLandingPageClosed];
}

@end
