//
//  ADSuyiCustomGDTInitialize.h
//  ADSuyiSDKDemo-iOS
//
//  Created by Suancai on 2023/3/3.
//  Copyright © 2023 陈坤. All rights reserved.
//

#import <ADSuyiSDK/ADSuyiCustomAdapterInitialize.h>

NS_ASSUME_NONNULL_BEGIN

@interface ADSuyiCustomAdapterGDTInitialize : ADSuyiCustomAdapterInitialize

@end

NS_ASSUME_NONNULL_END
