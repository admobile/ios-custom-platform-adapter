//
//  ADSuyiCustomTestInitialize.h
//  ADSuyiCustomPlatform
//
//  Created by Erik on 2021/8/25.
//

#import <ADSuyiSDK/ADSuyiCustomAdapterInitialize.h>

NS_ASSUME_NONNULL_BEGIN

/// 初始化
@interface ADSuyiCustomAdapterBUInitialize : ADSuyiCustomAdapterInitialize

@end

NS_ASSUME_NONNULL_END
