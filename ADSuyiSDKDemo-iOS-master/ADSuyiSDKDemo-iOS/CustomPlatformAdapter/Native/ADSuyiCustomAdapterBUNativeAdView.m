//
//  ADSuyiCustomAdapterTestNativeAdView.m
//  ADSuyiCustomPlatform
//
//  Created by Erik on 2021/9/7.
//

#import "ADSuyiCustomAdapterBUNativeAdView.h"

@interface ADSuyiCustomAdapterBUNativeAdView ()

@end

@implementation ADSuyiCustomAdapterBUNativeAdView

#pragma mark - setter

- (void)setBuNativeAdView:(BUNativeExpressAdView *)buNativeAdView{
    _buNativeAdView = buNativeAdView;
    [self addSubview:buNativeAdView];
}


#pragma mark - ADSuyiAdapterNativeAdViewDelegate

- (void)adsy_registViews:(NSArray<UIView *> *)clickViews {
    [self.buNativeAdView render];
    [self.delegate adsyCustomNativeViewRenderSuccess:self];
}

- (void)adsy_unRegistView {
    
}

- (ADSuyiAdapterRenderType)renderType {
    return ADSuyiAdapterRenderTypeExpress;
}

- (ADSuyiAdapterNativeAdData *)data {
    return nil;
}

- (nullable UIView *)adsy_mediaViewForWidth:(CGFloat)width {
    return nil;
}

- (BOOL)adsy_closeButtonExist {
    // 是否存在关闭按钮
    return YES;
}

#pragma mark - ADSuyiAdViewInfoProtocol

- (ADSuyiAdapterPlatform)adsy_platform {
    return @"toutiao";
}

@end
