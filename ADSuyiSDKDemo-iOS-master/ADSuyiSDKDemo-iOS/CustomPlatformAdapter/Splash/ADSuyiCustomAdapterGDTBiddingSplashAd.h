//
//  ADSuyiCustomAdapterGDTSplashAd.h
//  ADSuyiSDKDemo-iOS
//
//  Created by Suancai on 2023/3/6.
//  Copyright © 2023 陈坤. All rights reserved.
//

#import <ADSuyiSDK/ADSuyiCustomAdapterSplashAd.h>

NS_ASSUME_NONNULL_BEGIN

/// GDT 支持 Bidding 的写法
@interface ADSuyiCustomAdapterGDTBiddingSplashAd : ADSuyiCustomAdapterSplashAd

@end

NS_ASSUME_NONNULL_END
