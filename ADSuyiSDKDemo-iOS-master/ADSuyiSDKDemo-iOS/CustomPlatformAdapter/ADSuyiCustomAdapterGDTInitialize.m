//
//  ADSuyiCustomGDTInitialize.m
//  ADSuyiSDKDemo-iOS
//
//  Created by Suancai on 2023/3/3.
//  Copyright © 2023 陈坤. All rights reserved.
//

#import "ADSuyiCustomAdapterGDTInitialize.h"
#import <ADSuyiSDK/ADSuyiSDK.h>
#import <GDTMobSDK/GDTSDKConfig.h>
#import <ADSuyiSDK/ADSuyiCustomAdapterInitialize.h>

@implementation ADSuyiCustomAdapterGDTInitialize

+ (void)load {
    // 调用注册方法
    [self registPlatformInitializeClass:self forSdkName:@"gdt"];
}

+ (void)initAdSDKWithConfigInfo:(ADSuyiCustomAdapterRequestContext *)config {
    static NSString *_appid;
    if(config.appId && [_appid isEqualToString:config.appId]) {
        return;
    }
    _appid = config.appId.copy;
    // 初始化三方SDK
    [GDTSDKConfig initWithAppId:_appid];
    [GDTSDKConfig startWithCompletionHandler:^(BOOL success, NSError *error) {
        
    }];
}

// MARK: - ADSuyiAdapterInitializeProtocol

// 需要实现改方法 返回三方平台当前版本号
+ (NSString *)platformSDKVersion {
    // 12221：可以为当前修改时间去的时间，也可以直接返回版本号
    return [NSString stringWithFormat:@"%@.12221",[GDTSDKConfig sdkVersion]];
}


@end
