//
//  ADSuyiCustomAdapterGDTNativeAdView.m
//  ADSuyiSDKDemo-iOS
//
//  Created by apple on 2025/1/6.
//

#import "ADSuyiCustomAdapterGDTNativeAdView.h"

@implementation ADSuyiCustomAdapterGDTNativeAdView

#pragma mark - setter

- (void)setGdtNativeAdView:(GDTNativeExpressAdView *)gdtNativeAdView {
    _gdtNativeAdView = gdtNativeAdView;
    [self addSubview:gdtNativeAdView];
}


#pragma mark - ADSuyiAdapterNativeAdViewDelegate

- (void)adsy_registViews:(NSArray<UIView *> *)clickViews {
    [self.gdtNativeAdView render];
}

- (void)adsy_unRegistView {
    
}

- (ADSuyiAdapterRenderType)renderType {
    return ADSuyiAdapterRenderTypeExpress;
}

- (ADSuyiAdapterNativeAdData *)data {
    return nil;
}

- (nullable UIView *)adsy_mediaViewForWidth:(CGFloat)width {
    return nil;
}

- (BOOL)adsy_closeButtonExist {
    return YES;
}

#pragma mark - ADSuyiAdViewInfoProtocol

- (ADSuyiAdapterPlatform)adsy_platform {
    return @"gdt";
}

@end
