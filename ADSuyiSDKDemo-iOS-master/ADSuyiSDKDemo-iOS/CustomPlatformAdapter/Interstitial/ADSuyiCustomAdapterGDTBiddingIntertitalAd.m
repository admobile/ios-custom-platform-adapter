//
//  ADSuyiCustomAdapterGDTBiddingIntertitalAd.m
//  ADSuyiSDKDemo-iOS
//
//  Created by apple on 2025/1/3.
//  Copyright © 2025 apple. All rights reserved.
//

#import "ADSuyiCustomAdapterGDTBiddingIntertitalAd.h"
#import <GDTMobSDK/GDTUnifiedInterstitialAd.h>

@interface ADSuyiCustomAdapterGDTBiddingIntertitalAd () <GDTUnifiedInterstitialAdDelegate> {
    GDTUnifiedInterstitialAd *_interstitialAd;
    ADSuyiCustomAdapterInterstitalRequestContext *_context;
}

@end

@implementation ADSuyiCustomAdapterGDTBiddingIntertitalAd

// MARK: - Override

+ (void)load {
    [self registPlatformAdLoaderClass:self forSdkName:@"gdt" renderType:ADSuyiAdapterRenderTypeExpress];
    [self registPlatformAdLoaderClass:self forSdkName:@"gdt" renderType:ADSuyiAdapterRenderTypeExpressPro];
}

- (void)requestAdWithContext:(ADSuyiCustomAdapterInterstitalRequestContext *)context {
    _context = context;
    [self request:context];
}

- (void)biddingRequestWithContext:(ADSuyiCustomAdapterInterstitalRequestContext *)context{
    _context = context;
    [self request:context];
}

- (void)showInterstitalAdFromViewController:(UIViewController *)viewController {
    [_interstitialAd presentAdFromRootViewController:viewController];
}

- (void)biddingResult:(ADSuyiSDKSourceBiddingResult)result AllPrice:(NSArray *)allPrices {
    // 获取一价二价
    NSNumber *winnerPrice = 0;
    NSNumber *secondPrice = 0;
    if (allPrices.count > 0) {
        NSNumber *firstPrice = allPrices.firstObject;
        // 聚合平台存储为单位为：分，GDT 是以元返回，同时也需要已元上报
        winnerPrice = [NSNumber numberWithFloat:firstPrice.floatValue * 100];
    }
    if (allPrices.count > 1) {
        NSNumber *tempSecondPrice = allPrices[1];
        secondPrice = [NSNumber numberWithFloat:tempSecondPrice.floatValue * 100];
    }
    switch (result) {
        case ADSuyiSDKSourceBiddingResult_Sucess:
            [self sendWinNotificationWithWinnerPrice:[NSNumber numberWithInteger:_interstitialAd.eCPM] withSecondPrice:secondPrice];
            break;
        case ADSuyiSDKSourceBiddingResult_Failed:
            [self sendLossNotificationWithPrice:winnerPrice withRease:GDTAdBiddingLossReasonLowPrice withWinnerAdnID:@""];
            break;
        case ADSuyiSDKSourceBiddingResult_Timeout:
            [self sendLossNotificationWithPrice:@0 withRease:GDTAdBiddingLossReasonNoAd withWinnerAdnID:@""];
            break;
        default:
            [self sendLossNotificationWithPrice:@0 withRease:GDTAdBiddingLossReasonOther withWinnerAdnID:@""];
            break;
    }
}

// MARK: - Method

- (void)request:(ADSuyiCustomAdapterInterstitalRequestContext *)context {
    if(!_interstitialAd) {
        _interstitialAd = [[GDTUnifiedInterstitialAd alloc] initWithPlacementId:context.posId];
        _interstitialAd.delegate = self;
    }
    [_interstitialAd loadAd];
}

/** 竞赢上报
 *  infoDic 字典类型，支持的key有
 *  GDT_M_W_E_COST_PRICE：竞胜价格 (单位: 分)，值类型为NSNumber *
 *  GDT_M_W_H_LOSS_PRICE：最高失败出价，值类型为NSNumber *
 */
- (void)sendWinNotificationWithWinnerPrice:(NSNumber *)winnerPrice withSecondPrice:(NSNumber *)secondPrice{
    if(winnerPrice == nil){
        return ;
    }
    
    NSMutableDictionary *infoDic = [NSMutableDictionary dictionary];
    
    [infoDic setValue:winnerPrice forKey:GDT_M_W_E_COST_PRICE];
    if (secondPrice != nil) {
        [infoDic setValue:secondPrice forKey:GDT_M_W_H_LOSS_PRICE];
    }else{
        [infoDic setValue:@(0) forKey:GDT_M_W_H_LOSS_PRICE];
    }
    [_interstitialAd sendWinNotificationWithInfo:infoDic.copy];
}
/**
 *  竞败之后或未参竞调用
 *
 *  infoDic 竞败信息，字典类型，支持的key有
 *  GDT_M_L_WIN_PRICE ：竞胜价格 (单位: 分)，值类型为NSNumber *，选填
 *  GDT_M_L_LOSS_REASON ：优量汇广告竞败原因，竞败原因参考枚举GDTAdBiddingLossReason中的定义，值类型为NSNumber *，必填
 *  GDT_M_ADNID  ：竞胜方渠道ID，值类型为NSString *，必填
 */
- (void)sendLossNotificationWithPrice:(NSNumber *)price withRease:(GDTAdBiddingLossReason)reason withWinnerAdnID:(NSString *)winnerAdnID{
    NSMutableDictionary *infoDic = [NSMutableDictionary dictionary];
    [infoDic setValue:price forKey:GDT_M_L_WIN_PRICE];
    [infoDic setValue:@(reason) forKey:GDT_M_L_LOSS_REASON];
    [infoDic setValue:winnerAdnID forKey:GDT_M_ADNID];
    [_interstitialAd sendLossNotificationWithInfo:infoDic.copy];
}

// MARK: - GDTUnifiedInterstitialAdDelegate

- (void)unifiedInterstitialSuccessToLoadAd:(GDTUnifiedInterstitialAd *)unifiedInterstitial {
    NSInteger price = _interstitialAd.eCPM;
    [self trackInterstitialAdSuccessToLoad:price/100.00];
}

- (void)unifiedInterstitialFailToLoadAd:(GDTUnifiedInterstitialAd *)unifiedInterstitial error:(NSError *)error {
    [self trackInterstitialAdFailToLoadError:error];
}

- (void)unifiedInterstitialDidPresentScreen:(GDTUnifiedInterstitialAd *)unifiedInterstitial {
    [self trackInterstitialAdDidPresent];
}

- (void)unifiedInterstitialWillExposure:(GDTUnifiedInterstitialAd *)unifiedInterstitial {
    [self trackInterstitialAdDidExposure];
}

- (void)unifiedInterstitialFailToPresent:(GDTUnifiedInterstitialAd *)unifiedInterstitial error:(NSError *)error {
    [self trackInterstitialAdFailToPresentError:error];
}

- (void)unifiedInterstitialClicked:(GDTUnifiedInterstitialAd *)unifiedInterstitial {
    [self trackInterstitialAdDidClick];
}

- (void)unifiedInterstitialDidDismissScreen:(GDTUnifiedInterstitialAd *)unifiedInterstitial {
    [self trackInterstitialAdDidClose];
}

- (void)unifiedInterstitialAdDidDismissFullScreenModal:(GDTUnifiedInterstitialAd *)unifiedInterstitial {
    [self trackInterstitialAdLandingPageClosed];
}

@end
