//
//  ADSuyiCustomAdapterTestRewardAd.m
//  ADSuyiCustomPlatform
//
//  Created by Erik on 2021/9/6.
//

#import "ADSuyiCustomAdapterBURewardAd.h"
#import <BUAdSDK/BUNativeExpressRewardedVideoAd.h>
#import <BUAdSDK/BURewardedVideoModel.h>

@interface ADSuyiCustomAdapterBURewardAd ()<BUNativeExpressRewardedVideoAdDelegate>
{
    BUNativeExpressRewardedVideoAd *_rewardVideoAd;
    ADSuyiCustomAdapterRewardRequestContext *_context;
    BOOL _isReady;
}
@end

@implementation ADSuyiCustomAdapterBURewardAd

// MARK: - Override

+ (void)load {
    [self registPlatformAdLoaderClass:self forSdkName:@"toutiao" renderType:(ADSuyiAdapterRenderTypeExpress)];
}

/// 请求
- (void)requestAdWithContext:(ADSuyiCustomAdapterRewardRequestContext *)context {
    _context = context;
    BURewardedVideoModel *model= [BURewardedVideoModel new];
    
    model.userId = context.userId;
    if (context.extra){
        model.extra = context.extra;
    }
    if (context.rewardName){
        model.rewardName = context.rewardName;
    }
    if (context.rewardAmount){
        model.rewardAmount = context.rewardAmount.integerValue;
    }
    
    _rewardVideoAd = [[BUNativeExpressRewardedVideoAd alloc] initWithSlotID:context.posId rewardedVideoModel:model];
    _rewardVideoAd.delegate = self;
    _rewardVideoAd.rewardPlayAgainInteractionDelegate = self;
    [_rewardVideoAd loadAdData];
}

/// 展示
- (void)showRewardVodAdFromViewController:(UIViewController *)viewController {
    if (!viewController) {
        viewController = _context.viewController;
    }
    [_rewardVideoAd showAdFromRootViewController:viewController];
}

- (bool)isReady{
    return _isReady;
}

- (bool)isValid{
    return _isReady;
}

- (bool)canServerVerrify{
    return true;
}


// MARK: - Method

- (void)setIsReady:(BOOL)isReady {
    if(_isReady == NO && isReady == YES) {
        _isReady = isReady;
        [self trackRewardvodAdReadyToPlay];
    } else {
        _isReady = isReady;
    }
}


// MARK: - BUNativeExpressRewardedVideoAdDelegate

/**
 This method is called when video ad material loaded successfully.
 */
- (void)nativeExpressRewardedVideoAdDidLoad:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd {
    [self trackRwardvodAdLoadSuccess];
    self.isReady = true;
}

/**
 This method is called when video ad materia failed to load.
 @param error : the reason of error
 */
- (void)nativeExpressRewardedVideoAd:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd didFailWithError:(NSError *_Nullable)error {
    [self trackRewardvodAdFailToLoadError:error];
}

/**
 This method is called when cached successfully.
 For a better user experience, it is recommended to display video ads at this time.
 And you can call [BUNativeExpressRewardedVideoAd showAdFromRootViewController:].
 */
- (void)nativeExpressRewardedVideoAdDidDownLoadVideo:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd {
    [self trackRewardvodAdVideoLoadSuccess];
}

/**
 This method is called when rendering a nativeExpressAdView successed.
 It will happen when ad is show.
 */
- (void)nativeExpressRewardedVideoAdViewRenderSuccess:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd {}

/**
 This method is called when a nativeExpressAdView failed to render.
 @param error : the reason of error
 */
- (void)nativeExpressRewardedVideoAdViewRenderFail:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd error:(NSError *_Nullable)error {
    [self trackRewardvodAdFailToPresentError:error];
}

/**
 This method is called when video ad slot will be showing.
 */
- (void)nativeExpressRewardedVideoAdWillVisible:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd {
    [self trackRewardvodAdWillVisible];
}

/**
 This method is called when video ad slot has been shown.
 */
- (void)nativeExpressRewardedVideoAdDidVisible:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd {
    [self trackRewardvodAdDidVisible];
}

/**
 This method is called when video ad is about to close.
 */
- (void)nativeExpressRewardedVideoAdWillClose:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd {}

/**
 This method is called when video ad is closed.
 */
- (void)nativeExpressRewardedVideoAdDidClose:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd {
    [self trackRewardvodAdDidClose];
}

/**
 This method is called when video ad is clicked.
 */
- (void)nativeExpressRewardedVideoAdDidClick:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd {
    [self trackRewardvodAdDidClick];
}

/**
 This method is called when the user clicked skip button.
 */
- (void)nativeExpressRewardedVideoAdDidClickSkip:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd {}

/**
 This method is called when video ad play completed or an error occurred.
 @param error : the reason of error
 */
- (void)nativeExpressRewardedVideoAdDidPlayFinish:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd didFailWithError:(NSError *_Nullable)error {
    if(error){
        [self trackRewardvodAdVideoFailToPlayError:error];
    }else{
        [self trackRewardvodAdDidPlayFinish];
    }
}

/**
 Server verification which is requested asynchronously is succeeded. now include two v erify methods:
      1. C2C need  server verify  2. S2S don't need server verify
 @param verify :return YES when return value is 2000.
 */
- (void)nativeExpressRewardedVideoAdServerRewardDidSucceed:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd verify:(BOOL)verify {
    [self trackRewardVodAdDidRewardEffective];
    
    if(verify){
        [self trackRewardvodAdVideoAdServerRewardDidSucceedWithInfo:nil];
    }else{
        // 自定义
        NSError *error = [[NSError alloc]initWithDomain:@"custom.adapter.error" code:22052 userInfo:@{NSLocalizedDescriptionKey : @"服务端验证失败，请服务端正确响应穿山甲服务端"}];
        [self trackRewardvodAdVideoAdServerRewardDidFailed:error];
    }
}


/**
  Server verification which is requested asynchronously is failed.
  @param rewardedVideoAd express rewardVideo Ad
  @param error request error info
 */
- (void)nativeExpressRewardedVideoAdServerRewardDidFail:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd error:(NSError *_Nullable)error {
    [self trackRewardvodAdVideoAdServerRewardDidFailed:error];
}

/**
 This method is called when another controller has been closed.
 @param interactionType : open appstore in app or open the webpage or view video ad details page.
 */
- (void)nativeExpressRewardedVideoAdDidCloseOtherController:(BUNativeExpressRewardedVideoAd *)rewardedVideoAd interactionType:(BUInteractionType)interactionType {
    [self trackRewardvodAdLandingPageClosed];
}

@end
