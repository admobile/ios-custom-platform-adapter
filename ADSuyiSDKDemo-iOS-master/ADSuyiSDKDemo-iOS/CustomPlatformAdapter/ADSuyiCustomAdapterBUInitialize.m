//
//  ADSuyiCustomTestInitialize.m
//  ADSuyiCustomPlatform
//
//  Created by Erik on 2021/8/25.
//

#import "ADSuyiCustomAdapterBUInitialize.h"
#import <BUAdSDK/BUAdSDK.h>
#import <ADSuyiSDK/ADSuyiSDK.h>
#import <ADSuyiSDK/ADSuyiCustomAdapterInitialize.h>

@implementation ADSuyiCustomAdapterBUInitialize

+ (void)load {
    // 调用注册方法
    //以现有广告平台穿山甲作为demo示例
    [self registPlatformInitializeClass:self forSdkName:@"toutiao"];
}

// MARK: - Override

+ (void)initAdSDKWithConfigInfo:(ADSuyiCustomAdapterRequestContext *)config {
    static NSString *_appid;
    if(config.appId && [_appid isEqualToString:config.appId]) {
        return;
    }
    _appid = config.appId.copy;
    // 初始化三方SDK
    [BUAdSDKManager setAppID:_appid];
    
    BUAdSDKConfiguration *configuration = [BUAdSDKConfiguration configuration];
    configuration.territory = BUAdSDKTerritory_CN; // 需要设置
    if (![ADSuyiSDK enablePersonalAd]){
        NSString *userExtData = @"[{\"name\":\"personal_ads_type\",\"value\":\"0\"}]";
        configuration.userExtData = userExtData;
    }
    configuration.appID = _appid;
    configuration.secretKey = config.appKey;
    [BUAdSDKManager startWithAsyncCompletionHandler:^(BOOL success, NSError *error) {
        if (success) {
            // Success
            NSLog(@"toutiao sdk init success");
        }else{
            // Error
            NSLog(@"toutiao sdk init error = %@",error);
        }
    }];
}

// 需要实现改方法 返回三方平台当前版本号
+ (NSString *)platformSDKVersion {
    // 12221：可以为当前修改时间去的时间，也可以直接返回版本号
    return [NSString stringWithFormat:@"%@.12221",[BUAdSDKManager SDKVersion]];
}

@end
