//
//  ADSuyiCustomAdapterGDTNativeAdView.h
//  ADSuyiSDKDemo-iOS
//
//  Created by apple on 2025/1/6.
//

#import <ADSuyiSDK/ADSuyiCustomAdapterNativeAdView.h>
#import <GDTMobSDK/GDTNativeExpressAdView.h>

NS_ASSUME_NONNULL_BEGIN

@interface ADSuyiCustomAdapterGDTNativeAdView : ADSuyiCustomAdapterNativeAdView

@property (nonatomic, strong) GDTNativeExpressAdView *gdtNativeAdView;

@end

NS_ASSUME_NONNULL_END
