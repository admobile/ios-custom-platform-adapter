//
//  ADSuyiCustomAdapterTestUnifiedNativeAd.h
//  ADSuyiCustomPlatform
//
//  Created by Erik on 2021/9/8.
//

#import <ADSuyiSDK/ADSuyiCustomAdapterUnifiedNativeAd.h>

NS_ASSUME_NONNULL_BEGIN

@interface ADSuyiCustomAdapterGDTUnifiedNativeAd : ADSuyiCustomAdapterUnifiedNativeAd

@end

NS_ASSUME_NONNULL_END
