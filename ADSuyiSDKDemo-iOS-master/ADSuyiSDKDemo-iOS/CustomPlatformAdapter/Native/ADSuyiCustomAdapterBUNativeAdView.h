//
//  ADSuyiCustomAdapterTestNativeAdView.h
//  ADSuyiCustomPlatform
//
//  Created by Erik on 2021/9/7.
//

#import <ADSuyiSDK/ADSuyiCustomAdapterNativeAdView.h>
#import <BUAdSDK/BUNativeExpressAdView.h>

NS_ASSUME_NONNULL_BEGIN

@class ADSuyiCustomAdapterBUNativeAdView;
@protocol ADSuyiCustomAdapterBUNativeViewDelegate <NSObject>

- (void)adsyCustomNativeViewRenderSuccess:(ADSuyiCustomAdapterBUNativeAdView *)nativeView;

@end

@interface ADSuyiCustomAdapterBUNativeAdView : ADSuyiCustomAdapterNativeAdView

/// 平台信息流广告视图
@property (nonatomic, strong) BUNativeExpressAdView *buNativeAdView;

@property (nonatomic, weak) id<ADSuyiCustomAdapterBUNativeViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
