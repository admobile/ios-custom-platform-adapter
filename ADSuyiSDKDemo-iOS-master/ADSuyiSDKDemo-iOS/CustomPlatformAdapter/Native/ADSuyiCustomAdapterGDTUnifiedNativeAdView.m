//
//  ADSuyiCustomAdapterTestUnifiedNativeAdView.m
//  ADSuyiCustomPlatform
//
//  Created by Erik on 2021/9/7.
//

#import "ADSuyiCustomAdapterGDTUnifiedNativeAdView.h"

@interface ADSuyiAdapterNativeAdData()

- (instancetype)initWithTitle:(NSString *)title
                      content:(NSString *)content
          imageUrlStringArray:(NSArray<NSString *> *)imageUrlStringArray
                    iconImage:(nullable UIImage *)iconImage
                 iconImageUrl:(nullable NSString *)iconImageUrl
          shouldShowMediaView:(BOOL)shouldShowMediaView;

@end


@interface ADSuyiCustomAdapterGDTUnifiedNativeAdView ()
{
    ADSuyiAdapterNativeAdData *_data;
    CGFloat _width;
}
@end

@implementation ADSuyiCustomAdapterGDTUnifiedNativeAdView

#pragma mark - ADSuyiAdapterNativeAdViewDelegate

- (void)adsy_registViews:(NSArray<UIView *> *)clickViews {
    [self registerDataObject:_adData clickableViews:clickViews];
    if([self.unifiedNativeAdDelegate respondsToSelector:@selector(adsyCustomUnifiedNativeAdViewRender:)]) {
        [self.unifiedNativeAdDelegate adsyCustomUnifiedNativeAdViewRender:self];
    }
}

- (ADSuyiAdapterNativeAdData *)data {
    return _data;
}

- (nullable UIView *)adsy_mediaViewForWidth:(CGFloat)width {
    [self.mediaView muteEnable:YES];
    return self.mediaView;
}

- (void)adsy_platformLogoImageDarkMode:(BOOL)darkMode loadImageBlock:(void (^)(UIImage * _Nullable))block {
    if (!block) {
        return;
    }
    block(nil);
}

- (ADSuyiAdapterRenderType)renderType {
    return ADSuyiAdapterRenderTypeNative;
}

- (void)adsy_unRegistView {
    [self unregisterDataObject];
}

- (void)adsy_close {
    [self.unifiedNativeAdDelegate adsyCustomUnifiedNativeAdViewClose:self];
}

- (BOOL)adsy_closeButtonExist {
    return NO;
}

#pragma mark - Set

- (void)setAdData:(GDTUnifiedNativeAdDataObject *)adData {
    _adData = adData;
    NSArray *imageUrlStringArray = @[];
    if(_adData.imageUrl) {
        imageUrlStringArray = @[_adData.imageUrl];
    }
    _data = [[ADSuyiAdapterNativeAdData alloc] initWithTitle:_adData.title
                                                     content:_adData.desc
                                         imageUrlStringArray:imageUrlStringArray
                                                   iconImage:nil
                                                iconImageUrl:_adData.iconUrl
                                         shouldShowMediaView:_adData.isVideoAd];
}


#pragma mark - ADSuyiAdViewInfoProtocol

- (ADSuyiAdapterPlatform)adsy_platform {
    return @"gdt";
}

@end
