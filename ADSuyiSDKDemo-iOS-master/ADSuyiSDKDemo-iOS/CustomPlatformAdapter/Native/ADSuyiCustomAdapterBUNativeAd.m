//
//  ADSuyiCustomAdapterTestNativeAd.m
//  ADSuyiCustomPlatform
//
//  Created by Erik on 2021/9/7.
//

#import "ADSuyiCustomAdapterBUNativeAd.h"
#import <BUAdSDK/BUAdSDK.h>
#import "ADSuyiCustomAdapterBUNativeAdView.h"
@interface ADSuyiCustomAdapterBUNativeAd ()<BUNativeAdsManagerDelegate,BUNativeExpressAdViewDelegate,ADSuyiCustomAdapterBUNativeViewDelegate>
{
    BUNativeExpressAdManager *_nativeExpressAd;
    NSMapTable <BUNativeExpressAdView *, ADSuyiCustomAdapterBUNativeAdView *> *_weakMapTable;
    NSHashTable<ADSuyiCustomAdapterBUNativeAdView *> *_hashTable;
    ADSuyiCustomAdapterNativeRequestContext *_context;
}

@end

@implementation ADSuyiCustomAdapterBUNativeAd

// MARK: - Override

+ (void)load {
    [self registPlatformAdLoaderClass:self forSdkName:@"toutiao" renderType:(ADSuyiAdapterRenderTypeExpress)];
}

- (instancetype)init{
    self = [super init];
    if(self){
        _weakMapTable = [NSMapTable weakToWeakObjectsMapTable];
        _hashTable = [NSHashTable new];
    }
    return self;
}

- (void)requestAdWithContext:(ADSuyiCustomAdapterNativeRequestContext *)context {
    _context = context;
    if(!_nativeExpressAd) {
        BUAdSlot *slot = [BUAdSlot new];
        slot.ID = context.posId;
        slot.AdType = BUAdSlotAdTypeFeed;
        slot.position = BUAdSlotPositionFeed;
        BUSize *imageSize = [BUSize sizeBy:BUProposalSize_Feed690_388];
        slot.imgSize = imageSize;
        _nativeExpressAd = [[BUNativeExpressAdManager alloc] initWithSlot:slot
                                                                   adSize:CGSizeMake(context.adSize.width, 0)];
        _nativeExpressAd.delegate = self;
    }
    [_nativeExpressAd loadAdDataWithCount:context.loadCount];
}


// MARK: - Helper

- (NSArray<ADSuyiCustomAdapterBUNativeAdView *> *)creatNativeViewFrom:(NSArray<__kindof BUNativeExpressAdView *> *)views {
    NSMutableArray *dataArray = [NSMutableArray new];
    for (BUNativeExpressAdView *view in views) {
        ADSuyiCustomAdapterBUNativeAdView *nativeView = [ADSuyiCustomAdapterBUNativeAdView new];
        nativeView.buNativeAdView = view;
        view.rootViewController = _context.viewController;
        [dataArray addObject:nativeView];
        nativeView.delegate = self;
        [_weakMapTable setObject:nativeView forKey:view];
    }
    return dataArray.copy;
}

// MARK: - BUNativeExpressAdViewDelegate

/**
 * Sent when views successfully load ad
 */
- (void)nativeExpressAdSuccessToLoad:(BUNativeExpressAdManager *)nativeExpressAdManager views:(NSArray<__kindof BUNativeExpressAdView *> *)views {
    NSArray<ADSuyiCustomAdapterBUNativeAdView *> *adViewArray = [self creatNativeViewFrom:views];
    [self trackNativeAdSucceedToLoadWithNativeAdViews:adViewArray];
}

/**
 * Sent when views fail to load ad
 */
- (void)nativeExpressAdFailToLoad:(BUNativeExpressAdManager *)nativeExpressAdManager error:(NSError *_Nullable)error {
    [self trackNativeAdFailToLoadWithError:error];
}

/**
 * This method is called when rendering a nativeExpressAdView successed, and nativeExpressAdView.size.height has been updated
 */
- (void)nativeExpressAdViewRenderSuccess:(BUNativeExpressAdView *)nativeExpressAdView {
    ADSuyiCustomAdapterBUNativeAdView *adView = [_weakMapTable objectForKey:nativeExpressAdView];
    if(!adView)return;
    [_hashTable removeObject:adView];
    adView.frame = nativeExpressAdView.bounds;
    [self trackNativeAdRenderSucceedWithNativeView:adView];
}

/**
 * This method is called when a nativeExpressAdView failed to render
 */
- (void)nativeExpressAdViewRenderFail:(BUNativeExpressAdView *)nativeExpressAdView error:(NSError *_Nullable)error {
    ADSuyiCustomAdapterBUNativeAdView *adView = [_weakMapTable objectForKey:nativeExpressAdView];
    if(!adView)return;
    [_hashTable removeObject:adView];
    [self trackNativeAdRenderFailedWithNativeView:adView];
}

/**
 * Sent when an ad view is about to present modal content
 */
- (void)nativeExpressAdViewWillShow:(BUNativeExpressAdView *)nativeExpressAdView {
    [self trackNativeAdExposuredWithNativeView:[_weakMapTable objectForKey:nativeExpressAdView]];
}

/**
 * Sent when an ad view is clicked
 *
 */
- (void)nativeExpressAdViewDidClick:(BUNativeExpressAdView *)nativeExpressAdView {
    [self trackNativeAdClickedWithNativeView:[_weakMapTable objectForKey:nativeExpressAdView]];
}

/**
Sent when a playerw playback status changed.
@param playerState : player state after changed
*/
- (void)nativeExpressAdView:(BUNativeExpressAdView *)nativeExpressAdView stateDidChanged:(BUPlayerPlayState)playerState {
    
}

/**
 * Sent when a player finished
 * @param error : error of player
 */
- (void)nativeExpressAdViewPlayerDidPlayFinish:(BUNativeExpressAdView *)nativeExpressAdView error:(NSError *)error {
    
}

/**
 * Sent when a user clicked dislike reasons.
 * @param filterWords : the array of reasons why the user dislikes the ad
 */
- (void)nativeExpressAdView:(BUNativeExpressAdView *)nativeExpressAdView dislikeWithReason:(NSArray<BUDislikeWords *> *)filterWords {
    [self trackNativeAdClosedWithNativeView:[_weakMapTable objectForKey:nativeExpressAdView]];
}

/**
 * Sent after an ad view is clicked, a ad landscape view will present modal content
 */
- (void)nativeExpressAdViewWillPresentScreen:(BUNativeExpressAdView *)nativeExpressAdView {
    
}

/**
 This method is called when another controller has been closed.
 @param interactionType : open appstore in app or open the webpage or view video ad details page.
 */
- (void)nativeExpressAdViewDidCloseOtherController:(BUNativeExpressAdView *)nativeExpressAdView interactionType:(BUInteractionType)interactionType {
    [self trackNativeAdLandingPageClosedWithNativeView:[_weakMapTable objectForKey:nativeExpressAdView]];
}


/**
 This method is called when the Ad view container is forced to be removed.
 @param nativeExpressAdView : Ad view container
 */
- (void)nativeExpressAdViewDidRemoved:(BUNativeExpressAdView *)nativeExpressAdView {
    
}

-(void)adsyCustomNativeViewRenderSuccess:(ADSuyiCustomAdapterBUNativeAdView *)nativeView{
    [_hashTable addObject:nativeView];
}

@end
